import React from 'react'
import ArtPage from "../Components/ArtPage"
import renderer from 'react-test-renderer'

it('renders correctly', () => {
    const tree = renderer
        .create(<ArtPage pageId={0} />)
        .toJSON();
    expect(tree).toMatchSnapshot()
})
