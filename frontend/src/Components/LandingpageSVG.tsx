import React from 'react';

const LandingpageSVG = () => {

    const SVGpoints = ["150,75", "250,75", "325,150", "325,250", "250,325", "150,325", "75,250", "75,150"];

    const pickPoints = () => {
        let points = []
        while(points.length < 3) {
            let x = SVGpoints[Math.floor(Math.random() * SVGpoints.length)];
            while(points.indexOf(x) !== -1) {
                x = SVGpoints[Math.floor(Math.random() * SVGpoints.length)];
            }
            points.push(x);
        }
        return points.join(' ');
    }

    return(
        <div className={'welcome-svg'}>
            <svg id="SVG" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 400 400">
                <polygon points={pickPoints()} fill="blue" fill-opacity="0.5"  />
                <polygon points={pickPoints()} fill="red" fill-opacity="0.5" />
                <polygon points={pickPoints()} fill="green" fill-opacity="0.5" />
                <polygon points={pickPoints()} fill="yellow" fill-opacity="0.5" />
                <polygon points={pickPoints()} fill="pink" fill-opacity="0.5" />
                <polygon points={pickPoints()} fill="orange" fill-opacity="0.5" />
            </svg>
        </div>
    );

}

export default LandingpageSVG;


