import React from 'react';
import MainPage from './MainPage';
import '../Style/App.css';

const App = () => {
  return (
    <div className="App">
      <MainPage />
    </div>
  );
};

export default App;
