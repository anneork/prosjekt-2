import React, {useEffect, useState} from 'react';
import active from '../Media/favorite-active.png';
import inactive from '../Media/favorite.png';
import '../Style/FavoriteButton.css';

type props = {
    pageId: number;
}

const FavoriteButton = (props: props) => {
    const localStorage = window.localStorage; // Retrieve the local storage
    const pageId = props.pageId.toString(); // Needs to be string for local storage
    // Retrieve whether or not the current page is a favorite stored locally
    const initVal = localStorage.getItem(pageId) === 'true';
    const [isFavorite, setIsFavorite] = useState(initVal);

    useEffect(() => { // Runs whenever we change page. Check if current page is favorite
      const favorite = localStorage.getItem(pageId) === 'true';
      setIsFavorite(favorite); // Update the displayed favorite
    }, [pageId]);

    const toggleFavorite = () => { // Whenever user clicks the button
        const newValue = (!isFavorite).toString(); // Invert the current choice
        localStorage.setItem(pageId, newValue); // Update the storage
        setIsFavorite(newValue === 'true'); // Update the state
    };

    return (<div className={"favorite-button"}>
        <img
            src={isFavorite ? active : inactive}
            alt={"favorite-active"}
            style={{width: '10vh', height: '10vh'}}
            onClick={toggleFavorite}
        />
    </div>)
};

export default FavoriteButton;
