import React, {useState} from 'react';
import '../Style/DropDown.css';
import DropDownSelect from './DropDownSelect';
import Music from "./Music";

const DropDown = () => {
    const [clicked, setClicked] = useState(false);

    const BackgroundOptions = ["One", "Two", "Three", "Four"];
    const FontOptions = ['Calibri', 'Arial', 'Sans'];
    const MusicOptions = ["None", "Epic", "Energy", "Calm"];

    const determineRender = () => {
        if (clicked) return (
            <div className={"dropdown-menu"}>
                <span className={'cross-icon'} onClick={() => setClicked(!clicked)}/>
                <div className={'dropdowns'}>
                    <DropDownSelect
                        optionLabel={"Background"}
                        options={BackgroundOptions}
                    />
                    <DropDownSelect
                        optionLabel={"Font"}
                        options={FontOptions}
                    />
                    <DropDownSelect
                        optionLabel={"Sound"}
                        options={MusicOptions}
                    />
                </div>
                <p className={"royalty"}>Music: www.bensound.com</p>
            </div>
        );
        return <span className={'settings-icon'} onClick={() => setClicked(!clicked)}/>
    };

    return (<div className={'dropdown'}>
        <Music/>
        {determineRender()}
    </div>);
};

export default DropDown;