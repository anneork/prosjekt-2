import React from 'react';
import ExhibitionPiece from './ExhibitionPiece';
import FavoriteButton from './FavoriteButton';
import DropDown from './DropDown';
import NavButton from './NavButton';
import '../Style/ArtPage.css';
import '../Style/MainPage.css'
import Music from "./Music";


type props = {
    pageId: number; // The id of the page
}

type options = { [key: string]: string }; // Key string dictionary with different options
type state = {
    options: options;
    updateOption: (arg0: string, arg1: string) => void;
};
export const OptionsContext = React.createContext({} as state); // Context for passing down Options

class ArtPage extends React.Component<props, state> {

    updateToggleChoice = (label: string, choice: string) => { // Called when player updates the dropdown
        this.updateOptions({...this.state.options, [label]: choice}); // Updates state
        this.updateSessionStorage(label, choice); // Updates storage
    };

    updateOptions = (options: options) => { // Updates the state with newly received options
        this.setState(current => ({
            ...current,
            options
        }))
    };

    updateSessionStorage = (label: string, choice: string) => {
        const pageId = this.props.pageId.toString();
        const choices = sessionStorage.getItem(pageId); // Previously stored options. Saved as string
        if (choices != null) { // We have stored something previously
            const choicesObject: object = JSON.parse(choices); // Convert string to object
            let choicesObjectStringified : string = JSON.stringify({...choicesObject, [label]: choice});
            sessionStorage.setItem(pageId, choicesObjectStringified); // Update choices
        } else
            sessionStorage.setItem(pageId, JSON.stringify({...this.state.options, [label]: choice}));
    };

    componentDidUpdate(prevProps: Readonly<props>, prevState: Readonly<state>): void {
        const {pageId} = this.props; // No toString as it compares with prevState
        if (prevProps.pageId !== pageId) { // We have changed page. Must update the options
            const savedOptions = sessionStorage.getItem(pageId.toString());
            if (savedOptions !== null) { // Stored something previously
                const savedOptionsObject = JSON.parse(savedOptions); // Convert string to object
                this.updateOptions(savedOptionsObject);
            } else {
                this.updateOptions({...this.options});
            }
        }
    }

    options = {
        "sound": "none",
        "background": "one",
        "font": "calibri"
    };
    state = {
        options: {...this.options},
        updateOption: this.updateToggleChoice
    };

    render() {
        return (
            <div className={`artpage background-${this.state.options.background} font-${this.state.options.font}`}>
                <FavoriteButton pageId={this.props.pageId}/>
                <OptionsContext.Provider value={this.state}>
                    <DropDown/>
                </OptionsContext.Provider>
                <ExhibitionPiece artId={this.props.pageId}/>
                <div className={'navbutton left'}>
                    <NavButton direction={"left"}/>
                </div>
                <div className={'navbutton right'}>
                    <NavButton direction={"right"}/>
                </div>
            </div>
        );
    }
}

export default ArtPage;