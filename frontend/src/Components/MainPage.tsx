import React from 'react';
import '../Style/MainPage.css';
import LandingPage from './LandingPage';
import ArtPage from './ArtPage';

type props = {}

type state = {
    currentPage: number,
    totalPages: number
}

export const PageContext = React.createContext({
    changePage: (arg0: boolean) => {
    }, setPage: (arg0: number) => {
    }
});

class MainPage extends React.Component<props, state> {

    state = {currentPage: 0, totalPages: 8}; // Index of which page to render. 0 is the landing page

    determineRender() { // Decides what page will be rendered
        return this.state.currentPage === 0 ? <LandingPage/> :
            <ArtPage pageId={this.state.currentPage}/>;
    }

    changePage = (increment: boolean) => {
        const newPage = increment ? this.state.currentPage + 1 : this.state.currentPage - 1;
        if (newPage === -1)
            this.setState({currentPage: this.state.totalPages - 1}); // Negative modulo not supported
        else
            this.setState({currentPage: newPage % this.state.totalPages}); // Go to the new page or loop around if you've hit last page
    };

    setPage = (pageId: number) => {
        this.setState({currentPage: pageId});
    };

    render() {
        return (
            <div className={'main-page'}>
                <div className={'body'}>
                    <PageContext.Provider value={{
                        changePage: this.changePage,
                        setPage: this.setPage,
                    }}>
                        {this.determineRender()}
                    </PageContext.Provider>
                </div>
            </div>
        );
    }
}

export default MainPage;
