import React, {useContext} from "react";
import energy from "../Media/bensound-energy.mp3";
import epic from "../Media/bensound-epic.mp3";
import calm from "../Media/bensound-thejazzpiano.mp3";
import {OptionsContext} from "./ArtPage";

const Music = () => {
    const context = useContext(OptionsContext);

    const determineRender = () => {
        switch (context.options.sound) {
            case "energy":
                return energy;
            case "epic":
                return epic;
            case "calm":
                return calm;
            default:
                return "";
        }
    };

    return (
        <div>
            <audio src={determineRender()} autoPlay/>
        </div>
    );
};

export default Music;
