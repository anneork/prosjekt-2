import React from 'react';
import usePoetry from './usePoetry';
import SVGs from '../SVGs';

type props = {
  artId: number;
}

const ExhibitionPiece = (props: props) => {
  const [title, poem, author] = usePoetry(props.artId-1); // Calls on the usePoetry hook to retrieve the poem corresponding to Id

  const renderPoems = () => {
    if (poem != null)
      return poem.map(line => <p>{line}</p>);
    return <p>Loading...</p>
  };

  return (
    <div className={'exhibition-piece'}>
      <div className={"exhibition-piece-title"}>
          <h1>{title}</h1>
      </div>
      <div className={"author"}>
        {author}
      </div>
      <div className={"svg-artwork"}>
        {SVGs[props.artId-1]}
      </div>
      <div className={"poem"}>
        {renderPoems()}
      </div>
    </div>
  );
};

export default ExhibitionPiece;
