import React, {useContext} from 'react';
import {PageContext} from "./MainPage";
import leftarrow from "../Media/left-arrow.png";
import rightarrow from "../Media/right-arrow.png";

type props = {
  direction: string;
}

const NavButton = (props: props) => {
  const context = useContext(PageContext);
  console.log(context);

  const onClick = () => {
    const increment: boolean = props.direction === "right";
    context.changePage(increment);
  };

  return (
    <img
      src={props.direction === "right" ? rightarrow : leftarrow}
      alt={props.direction}
      style={{ width: '10vh', height: '10vh' }}
      onClick={onClick}
    />
  )
};

export default NavButton;
