import React, {useContext} from "react";
import {PageContext} from "./MainPage";

type props = {
    id: number,
};

const FavoriteLink = (props: props) => {
    const context = useContext(PageContext);
    return (<li className={"fav-link"}>
        <p onClick={() => context.setPage(props.id)}>{props.id}</p>
    </li>)
};

export default FavoriteLink;