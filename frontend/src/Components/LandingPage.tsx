import React, { ReactElement} from 'react';
import NavButton from './NavButton';
import "../Style/LandingPage.css"
import "../Style/MainPage.css"
import FavoriteLink from "./FavoriteLink";
import LandingpageSvg from "./LandingpageSVG";

type props = { // Typescript definitions for the arguments received via props
}

type state = { // Typescript definitions for the state variables of the class
    favorites: Array<ReactElement>;
}

class LandingPage extends React.Component<props, state> {

    constructor(props: props) {
        super(props); // Allows access to props in other functions
        this.state = {favorites: []}
    }

    

    componentDidMount(): void {
        const favs = [];
        for (let i = 1; i < 12; i++) {
            const favorite = window.localStorage.getItem(i.toString()) === 'true';
            if (favorite) favs.push(<FavoriteLink id={i}/>);
        }
        this.setState({favorites: favs})
    }

    render() {
        return (
            <div className={'landing-page'}>
                <h1 className={'title'}> Welcome to the gallery!</h1>
                <div className={'welcome-svg'}>
                    <LandingpageSvg />
                </div>
                <p className={'welcome-p'}>Welcome to this interactive gallery! Here you will find unique art pieces,
                    consisting of a quote and SVG. By clicking on the settings icon you can turn on some music and get your desired look! You can also
                    save these settings for future appretiation of the art pieces.
                </p>
                <div className={'favorites'}>
                    <p>Favorites:</p>
                    <ul>{this.state.favorites}</ul>
                </div>
                <div className={'flex2'}>
                <div className={'navbutton left'}>
                    <NavButton direction={"left"}/>
                </div>
                <div className={'navbutton right'}>
                    <NavButton direction={"right"}/>
                </div>
                </div>
            </div>
        );
    }
}

export default LandingPage;
