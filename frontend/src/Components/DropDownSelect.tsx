import React, {useContext, useState} from 'react';
import {OptionsContext} from "./ArtPage";

type props = {
    options: Array<string>; // Depends on if the user is choosing strings or numbers
    optionLabel: "Background" | "Sound" | "Font";
};

const DropDownSelect = (props: props) => {
    const context = useContext(OptionsContext); // Uses context defined in ArtPage
    const currentlySelected: string = context.options[props.optionLabel.toLowerCase()];
    const [value, setValue] = useState(currentlySelected); // The currently submitted element

    const handleChange = (event: React.ChangeEvent<HTMLSelectElement>) => { // Called whenever user changes choice
        event.preventDefault(); // To prevent default HTML behavior
        const val = event.currentTarget.value;
        setValue(val); // Update the value with the newly selected one
        context.updateOption(props.optionLabel.toLowerCase(), val.toLowerCase()); // Calls the function of the parent with the submitted value
    };

    const renderOptions = () => { // To render a varying amount of options
        return props.options.map(opt => <option value={opt} key={opt}>{opt}</option>);
    };

    return (<div className={'dropdown-toggle'}>
        <form>
            <label>
                {props.optionLabel}
                <select value={value} onChange={handleChange}>
                    {renderOptions()}
                </select>
            </label>
        </form>
    </div>);
};

export default DropDownSelect;