import React from "react";
import {ReactComponent as SVG1} from './SVG/svg1.svg';
import {ReactComponent as SVG2} from "./SVG/svg2.svg";
import {ReactComponent as SVG3} from "./SVG/svg3.svg";
import {ReactComponent as SVG4} from "./SVG/svg4.svg";
import {ReactComponent as SVG5} from "./SVG/svg5.svg";
import {ReactComponent as SVG6} from "./SVG/svg6.svg";
import {ReactComponent as SVG7} from "./SVG/svg7.svg";

export default [ // Collection of Artworks that will be displayed
    <SVG1/>,
    <SVG2/>,
    <SVG3/>,
    <SVG4/>,
    <SVG5/>,
    <SVG6/>,
    <SVG7/>,
];
