## Functionality
### Artworks
All of the artworks are made and animated using SVGs. This is because they scale better than canvas and are require less code to animate. They have the same aspect ratio so that they all fit in our page layout, and use viewbox to scale with the window size. 

### Sound
You can choose between three different soundtracks to each exhibition piece. These are royalty free songs gathered from www.bensound.com. We felt it made more sense to only switch between the three same tracks rather than three different tracks for each exhibition piece. This is partly due to the amount of files the user would have to download, but also the increased complexity of implementation. As Google Chrome has blocked autoplay of audio we decided to put the controls for starting, changing and stopping the music in the settings drop-down menu.

### Poetry
Poetry is retrieved using REST from PoetryDB.org.


## Technologies

### React 
React has been utilized throughout the project to achieve a responsive web page. The site has been split up to minimal components, which are then combined into larger components. We have mostly used functional components, though the Page components are implemented as classes. Had it not been a requirement, we would have likely only used functional components with hooks. This is because they require less boilerplate code and are more readable than their class counterparts. Every React file is .tsx file, meaning it is implemented with TypeScript. Context has been utilized in order to send props down several components without explicitly having to do so. Primarily, it has been used to switch pages on navigation button clicks, and to change the current page based on the selected dropdown options. Since our component structure is relatively simple, context has not been the most crucial addition to the project. If we had a much higher component hierarchy, it would have been much more essential. 
The project is initialized with npx and create-react-app. Alternatively we could have used Yarn, but this project requires the use of NPM. The end result would have been more or less the same.  


### Ajax
As only a simple get request is necessary for this project, we have used the standard fetch() function from JavaScript. Every time a new page is loaded, a new get request is sent to the poetryDB with an updated author argument. Alternatively, we could have fetched all poems at once, and then displayed them later on. Our solution requires less storage on the user side, but in turn results in many more API requests to the DB. On a larger scaled app, one would have to consider which drawback is worse. 


### Web Storage
Sessionstorage has been used to saved the selected options for each exhibition piece. Upon ending a session, each exhibition piece is reset to the standard value. Localstorage has been used to save favorite artworks. These persist upon different sessions, and thus you can easily navigate to your favorite pages. The feature of each storage type could have been swapped, though we felt this made the most sense. Favorites are typically something you want to store permanently, while display options can be temporary.

### Response Web Design
To make the website responsive we have used media queries and scalable elements. The site scales well for different screen sizes because the SVGs are in a scalable viewbox. We found that iPad aspect ratios are not too far from PC screen aspect ratios, so we have not made specific media queries for tablets. We have queries both for vertical and horizontal phones and have focused on iPhone screen sizes. The vertical queries changes the layout to a strictly vertical one, while the horizontal queries uses more columns and less rowns in the grid to take advantage of the wide aspect ratio. 

### Testing
We have tested that our page behaves appropriately in different browsers with different window sizes. When testing mobile screen sizes (vertical and horizontal) we have used the browser built-in inspect mode that represents common screen sizes for mobile phones. We also tested the website on actual phones during the last meeting and found it worked as intended.

Regarding snapshot testing, there is one simple snapshot test for rendering the art page. Since the project description specified “Prosjektet skal vise oppsett av og eksempel på testing med Jest - minimum er å ha en snapshottest" we included a single snapshot test. This was a learning experience since no one in the group had any experience with testing in react. To run the test we used jest and react test renderer. 

### Gits
We have used different branches and issues to complement our workflow. If we implemented features that required more than one commit, we would branch off the master branch and implement those changes there. This ensured that the master branch had stable changes. On merges with master, the relevant issues for the merge request should be referenced. Optimally, we would have only referenced the issue on merges, though we have been somewhat inconsistent here. 
